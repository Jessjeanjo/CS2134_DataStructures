//
//  JessicaJ_hw06.cpp
//
//
//  Created by Jessica Joseph on 11/07/15.
//
//

#include <iostream>
#include <string>
#include <stack>

using namespace std;


//====================================== PART ONE ======================================//


//1. Efficiently implement a queue class called Queue using a singly linked list, with no header or tail nodes. Your class
//should have the methods: front, back, empty, enqueue, dequeue.

template <class Object>
class Queue {
    
    
public:
    
    Object& back();
    Object& front();
    
    void enqueue(Object newItem); //push
    void dequeue(); //pop
    void empty();
    
    
};






template <typename Object>
class List
{
private:
    
    struct Node
    {
        Object  data;
        Node   *next;
        
        Node( const Object & d = Object{ },  Node * n = nullptr )
        : data{ d },  next{ n } { }
        
        Node( Object && d, Node * n = nullptr )
        : data{ std::move( d ) }, next{ n } { }
    };
    
public:
    class iterator
    {
    public:
        
        iterator( ): current( nullptr )
        { }
        
        Object & operator* ( )
        { return current->data; }
        
        const Object & operator* ( ) const
        { return  current->data; }
        
        iterator & operator++ ( )
        {
            this->current = this->current->next;
            return *this;
        }
        
        iterator operator++ ( int )
        {
            iterator old = *this;
            ++( *this );
            return old;
        }
        
        bool operator== ( const iterator & rhs ) const
        { return current == rhs.current; }
        
        bool operator!= ( const iterator & rhs ) const
        { return !( *this == rhs ); }
        
    private:
        Node * current;
        
        iterator( Node *p ) : current{ p }
        { }
        
        friend class List<Object>;
    };
    
public:
    List( )
    { header = nullptr; }
    
    ~List( )
    {
        clear( );
        delete header;
    }
    
    List( const List & rhs );
    
    List & operator= ( const List & rhs )
    {
        List copy = rhs;
        std::swap( *this, copy );
        return *this;
    }
    
    List( List && rhs ):header{ new Node }
    {
        header->next = rhs.header->next;
        rhs.header->next = nullptr;
    }
    
    List & operator= ( List && rhs )
    {
        std::swap( header, rhs.header );
        return *this;
    }
    
    iterator begin( ) const
    { return iterator( header->next ); }
    
    iterator end( ) const
    { return iterator( nullptr ); }
    
    iterator before_begin( ) const
    { return iterator( header ); }
    
    bool empty( ) const
    { return header->next == nullptr; }
    
    void clear( )
    {
        while( !empty( ) )
            pop_front( );
    }
    
    void pop_front( )
    { erase_after( before_begin( ) ); }
    
    iterator insert_after( iterator itr, const Object & x )
    {
        Node *p = itr.current;
        p->next = new Node{ x, p->next };
        
        return iterator(p->next);
    }
    
    void remove( const Object & x )
    {
        Node * prev = header;
        
        while ( prev->next != nullptr )
        {
            if ( prev->next->data == x )
                erase_after( iterator(prev) );
            else
                prev = prev->next;
        }
    }
    
    iterator erase_after( iterator itr )
    {
        Node *p = itr.current;
        Node *oldNode = p->next;
        
        p->next = oldNode->next;
        delete oldNode;
        
        return iterator( p->next );
    }
    
    Object & front( );
    
    const Object & front( ) const;
    
    void merge( List & alist );
    
    void reverse( );
    
    template<class Predicate>
    void remove_if( Predicate pred );
    
    iterator insert_after( iterator itr, Object && x );
    
private:
    Node *header;
    
};

//====================================== PART TWO ======================================//

bool balancedParen(string parens){
    //takes one argument brackets of type string. You may assume that all the characters in the string bracket
    //are one of the following characters: ( ) [ ] { }
    //returns true if the string contains properly nested and balanced parentheses, and false otherwise
    
    //If the string passed through then returns true.
    
    stack<char> thisStack;
    char compare;
    
    for (int i = 0; i < parens.length(); i++){
        
        if ((parens[i] == '(') || (parens[i] == '{') || (parens[i] == '[')){
            thisStack.push(parens[i]);
        } else {
            if (thisStack.empty()){
                return false;
            } else {
                compare = thisStack.top();
                if ((parens[i] == ')' && compare != '(') || (parens[i] == '}' && compare != '{') || (parens[i] == ']' && compare != '[')){
                    return false;}
                thisStack.pop();
            }
        }
    }
    
    if (thisStack.empty())
        return true;
    else
        return false;
    
}

int main ( ){
    

    
    
    
    cout << "\n\n//********************************* PART ONE *********************************//\n\n" << endl;
    cout << "\n\n//********************************* PART TWO *********************************//\n\n" << endl;
    cout <<    balancedParen("") << endl;
    
    
}