//
//  JessicaJ_hw03.cpp
//  
//
//  Created by Jessica Joseph on 9/28/15.
//
//

#include <iostream>
#include <vector>

using namespace std;

//================VECTOR CLASS================//
//Erase Method is right before the private variable declaration

template <typename Object>
class Vector
{
public:
    explicit Vector( int initSize = 0 )
    : theSize{ initSize }, theCapacity{ initSize + SPARE_CAPACITY }
    { objects = new Object[ theCapacity ]; }
    
    Vector( const Vector & rhs )
    : theSize{ rhs.theSize }, theCapacity{ rhs.theCapacity }, objects{ nullptr }
    {
        objects = new Object[ theCapacity ];
        for( int k = 0; k < theSize; ++k )
            objects[ k ] = rhs.objects[ k ];
    }
    
    Vector & operator= ( const Vector & rhs )
    {
        Vector copy = rhs;
        std::swap( *this, copy );
        return *this;
    }
    
    ~Vector( )
    { delete [ ] objects; }
    
    Vector( Vector && rhs )
    : theSize{ rhs.theSize }, theCapacity{ rhs.theCapacity }, objects{ rhs.objects }
    {
        rhs.objects = nullptr;
        rhs.theSize = 0;
        rhs.theCapacity = 0;
    }
    
    Vector & operator= ( Vector && rhs )
    {
        std::swap( theSize, rhs.theSize );
        std::swap( theCapacity, rhs.theCapacity );
        std::swap( objects, rhs.objects );
        
        return *this;
    }
    
    bool empty( ) const { return size( ) == 0; }
    int size( ) const { return theSize; }
    int capacity( ) const { return theCapacity; }
    
    Object & operator[]( int index )
    {
        return objects[ index ];
    }
    
    const Object & operator[]( int index ) const
    {
        return objects[ index ];
    }
    
    void resize( int newSize )
    {
        if( newSize > theCapacity )
            reserve( newSize * 2 );
        theSize = newSize;
    }
    
    void reserve( int newCapacity )
    {
        if( newCapacity < theSize )
            return;
        
        Object *newArray = new Object[ newCapacity ];
        for( int k = 0; k < theSize; ++k )
            newArray[ k ] = std::move( objects[ k ] );
        
        theCapacity = newCapacity;
        std::swap( objects, newArray );
        delete [ ] newArray;
    }
    
    // Stacky stuff
    void push_back( const Object & x )
    {
        if( theSize == theCapacity )
            reserve( 2 * theCapacity + 1 );
        objects[ theSize++ ] = x;
    }
    // Stacky stuff
    void push_back( Object && x )
    {
        if( theSize == theCapacity )
            reserve( 2 * theCapacity + 1 );
        objects[ theSize++ ] = std::move( x );
    }
    
    // Iterator stuff: not bounds checked
    typedef Object * iterator;
    typedef const Object * const_iterator;
    
    iterator begin( ) { return &objects[ 0 ]; }
    const_iterator begin( ) const { return &objects[ 0 ]; }
    iterator end( )   { return &objects[ size( ) ]; }
    const_iterator end( ) const { return &objects[ size( ) ]; }
    
    static const int SPARE_CAPACITY = 2;
    

    //==========================================HOMEWORK==========================================//
//================ERASE METHOD================//
//    iterator erase(iterator position){
//    
//    Vector returnVec(theSize - 1);
//    
//    for (int i = 0; i < returnVec.size(); i++){
//        if (i < *position)
//            returnVec.objects[i] = objects[i];
//        
//        else
//            returnVec.objects[i] = objects[i+1];
//    }
//    
//    objects = returnVec.objects;
//    theSize = returnVec.size();
//    theCapacity = returnVec.capacity();
//    
//    returnVec.objects = nullptr;
//        
//    return begin();
//    
//}
//    
//    iterator erase(iterator position){
//        
//        Vector returnVec(theSize - 1);
//        
//        iterator valTemp = position+1;
//        
//        for (iterator temp = position; temp++ != NULL; temp++){
//            objects[*temp] = *valTemp;
//        }
//
//        for (int i = 0; i < returnVec.size(); i++){
//            if (i < *position)
//                returnVec.objects[i] = objects[i];
//            
//            else
//                returnVec.objects[i] = objects[i+1];
//        }
//        
//        objects = returnVec.objects;
//        theSize = returnVec.size();
//        theCapacity = returnVec.capacity();
//        
//        returnVec.objects = nullptr;
        
//        return position;
//        
//    }
    
    iterator erase(iterator vItr) {
        
        // Check if vItr is valid
        if(vItr < begin() || vItr >= end()){ return end(); }
        
        // Shift elements after vItr over by one
        for(Vector<Object>::iterator itr = vItr; itr < (end()-1); itr++)
            *itr = *(itr+1);
        
        // Update the size, since we removed one element
        theSize--;
        
        // Returns the position the iterator pointed to
        // Note that this isn't invalidated, since all we did was shift over the values
        return vItr;
    }

private:
    int theSize;
    int theCapacity;
    Object * objects;
    
    
};


//================TEST================//

int main ()
{
    Vector<int> myvector;
    
    // set some values (from 1 to 10)
    for (int i=1; i<=10; i++) myvector.push_back(i);
    
    cout << "My Vector before using the Erase method:";
    for (unsigned i=0; i<myvector.size(); ++i)
        std::cout << ' ' << myvector[i];
    std::cout << '\n';
    
    int vecRemove = rand() % 9;
    
    cout << "I am now going to erase the " << vecRemove << "th element" << endl;
    myvector.erase (myvector.begin()+vecRemove);
    
    cout << "\nViola, the " << vecRemove << "th element of the old vector has been removed! \nMy new vector is:";
    for (unsigned i=0; i<myvector.size(); ++i)
        cout << ' ' << myvector[i];

    return 0;
}




