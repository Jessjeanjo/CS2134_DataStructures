//
//  JessicaJ_hw05.cpp
//  
//
//  Created by Jessica Joseph on 10/14/15.
//
//

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <ctime>

using namespace std;


//====================================== PART TWO ======================================//

// Create a functor called meFirst which has a private member variable, me, of type string. Its constructor
// will take one argument of type string that it uses to initialize its private private member variable, me.
// The functor’s overloaded operator() takes two arguments of type student and returns a boolean value


class Student {
public:
    Student (const string& who) : name(who) {}
    string getName () const { return name; }
private:
    string name;
};



class meFirst {
    string me;
    
public :
    meFirst(string meVal): me(meVal) {}
    
    bool operator()(Student studOne, Student studTwo){
        // Takes two parameters of type Student
        // Returns true if the first student's name is less than the second student
        
        // Testing whether either name is equivalent to "me" if so, the return value is immediately known
        if (studOne.getName() == me)
            return true; // True if the first student is equal since that automatically makes it the lesser value (what is being tested)
        if (studTwo.getName() == me)
            return false; // False if the second student is equal since that automatically makes it the lesser value
        
        
        if (studOne.getName() < studTwo.getName()) // Less than operator is overloaded in the string class which compares the individual characters
            return true;
        else
            return false;
    }
    
};


//====================================== PART THREE ======================================//

// Write a generic function template to enter data from the file ENABLE.txt into a: (a) vector<string> (b) set<string> (c) unordered set<string> (d) list<string>
// Remember that each of these containers has an insert method


string FILENAME = "testy.txt";

ifstream openFile(string errorMess = "Your file was not found, please try again!"){
    string fileName = FILENAME;
    
    ifstream inputFile;
    inputFile.open(FILENAME);
    
    while (!inputFile){
        cout << errorMess << endl;
        getline(cin, fileName);
        
        inputFile.clear();
        
        ifstream inputFile;
        inputFile.open(fileName);
    }
    
    cout << "File is opened!" << endl;
    return inputFile;
}


template < class Type>
void storeData(Type & fillType){

    ifstream fileHandle = openFile();
    string newData;
    
    while (fileHandle >> newData){
        fillType.push_back(newData);
    }
    
}


//======================================= PART FOUR ======================================//

// For each container, determine how long it takes to return an iterator to the word luge.
// For the vector and list class, use the STL algorithm lower bound.
// For the set and unordered set class, use the container’s method find.


class timer {
public:
    timer() : start(clock()) {}
    double elapsed() { return ( clock() - start ) / CLOCKS_PER_SEC; }
    void reset() { start = clock(); }
private:
    double start;
};


timer t;
double duration = t.elapsed();

string testWord = "luge";

// look up lower bound algorithms
// look up how to do a find within sets andunordered sets.
// also double check whether you can make open the ENABLE file!!!

int main(){
    
//    cout << "//======================================= PART TWO =======================================//" << endl;
//    
//    vector<Student> compVec{Student("Lisa"),Student("Jessica"), Student("Marie"), Student("Apple")};
//    
//    meFirst myComp("Monty") ;
//    
//    cout << myComp(compVec[0],compVec[1]);
//    cout << myComp(compVec[1],compVec[2]);
    
    
//    cout << "//====================================== PART THREE ======================================//" << endl;
//    
//    vector<string> thisVec;
//    storeData(thisVec);
//    
//    
//    cout << "I'm finished storing, spicy ball of awesomeness boss" << endl;
//    
//    for (int i = 0; i < 10; i++)
//        cout << thisVec[i] << endl;
//    
//    cout << "Finished presenting a few values that were stored, boss" << endl;
    
    cout << "//======================================= PART FOUR ======================================//" << endl;


    
}





