//
//  JessicaJ_hw02.h
//  
//
//  Created by Jessica Joseph on 9/17/15.
//
//

#ifndef ____JessicaJ_hw02__
#define ____JessicaJ_hw02__

#include <iostream>
#include <vector>
#include <string>
#include <fstream>

#endif /* defined(____JessicaJ_hw02__) */


using namespace std;


class TrainStopData{
    string stop_id;
    string stop_name;
    double stop_lat;
    double stop_long;

public:
    TrainStopData();
    TrainStopData(string ID, string name, string lat, string lon);
    
    string getStopID() const {return stop_id;}
    string getStopName() const {return stop_name;}
    double getStopLat() const {return stop_lat;}
    double getStopLon() const {return stop_long;}
    
};


TrainStopData::TrainStopData(){
    stop_id = "";
    stop_name = "";
    stop_lat = 0;
    stop_long = 0;
    
}

TrainStopData::TrainStopData(string ID, string name, string lat, string lon){
    stop_id = ID;
    stop_name = name;
    stop_lat = double(lat);
    stop_long = double(lon);
    
}