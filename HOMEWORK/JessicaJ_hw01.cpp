//
//  JessicaJ_hw01.cpp
//
//
//  Created by Jessica Joseph DUE: 9/13/15 11:55pm
//
//

#include "JessicaJ_hw01.h"

void partOne(){
    //Variable Declaration
    timer t;
    double nuClicks;
    
    vector<int> vecTesty;
    int const RAN_MAX = 1000, ARR_MAX =6;
    int returnVal;
    
    int arr[ARR_MAX] {int(pow(2,7)), int(pow(2,8)), int(pow(2,9)), int(pow(2,10)), int(pow(2,11)), int(pow(2,12))}; //Removes the need for a user to input the values
    
    cout.precision(numeric_limits<double>::digits10 + 1);
    cout << "********************************************PROGRAMMING PROBLEM ONE********************************************"<< endl;

    //For loop iterating over the different values of n
    for (int i = 0; i< ARR_MAX; i++){
        
        //For loop initializing  the vector based on the value for n
        for (int j = 0; j < arr[i]; j++)
            vecTesty.push_back((rand() % 2001) - RAN_MAX);
        
        cout << "STARTING RUNTIME FOR, n ="<< arr[i] <<endl;
        
//**********************************
        cout << "Cubic Maximum Contiguous Seubsequence Sum Algorithm" <<endl;
        //Starting cubic maximum contiguous subsequence sum algorithm and time elaspe
        t.reset();
        returnVal = maxSubSum1(vecTesty);
        nuClicks = t.elapsed();
        
        //Display results
        cout << "I am finished, boss!\nThis section was running for "<< nuClicks << " seconds."<< endl;
        cout << "The sum is " << returnVal << endl<<endl;
        
//**********************************
        cout << "Quadratic Maximum Contiguous Subsequence Sum Algorithm" <<endl;
        //Starting Quadratic maximum contiguous subsequence sum algorithm and time elaspe
        returnVal = 0;
        t.reset();
        returnVal = maxSubSum2(vecTesty);
        nuClicks = t.elapsed();
        
        cout << "I am finished, boss!\nThis section was running for "<< nuClicks << " seconds."<< endl;
        cout << "The sum is " << returnVal << endl<<endl;
        
        
//**********************************
        cout << "Linear-time Maximum Contiguous Subsequence Sum Algorithm" <<endl;
        //Starting linear-time maximum contiguous subsequence sum algorithm and time elaspe
        returnVal = 0;
        t.reset();
        returnVal = maxSubSum4(vecTesty);
        nuClicks = t.elapsed();
        
        cout << "I am finished, boss!\nThis section was running for "<< nuClicks << " seconds."<< endl;
        cout << "The sum is " << returnVal << endl<<endl;
        cout << "*************************************************************\n\n" << endl;
        
    }
    
}

void partTwo(){
    //Variable Declaration
    timer t;
    double nuClicks;
    
    vector<int> vecTesty;
    int const RAN_MAX = 1000, ARR_MAX =5;
    int sum =0;
    
    int arr[ARR_MAX] {int(pow(2,8)), int(pow(2,9)), int(pow(2,10)), int(pow(2,11)), int(pow(2,12))}; //Removes the need for a user to input the values
    
    cout.precision(numeric_limits<double>::digits10 + 1);
    cout << "********************************************PROGRAMMING PROBLEM TWO********************************************"<< endl;

    //For loop iterating over the different values of n
    for (int i = 0; i< ARR_MAX; i++){
        
        //For loop initializing  the vector based on the value for n
        for (int j = 0; j < arr[i]; j++)
            vecTesty.push_back((rand() % 2001) - RAN_MAX);
        
        cout << "STARTING RUNTIME FOR, n ="<< arr[i] <<endl;
        
        
        //**********************************
        cout << "CODE SNIPPET A" <<endl;
        t.reset();
        
        for (int i = 0; i < arr[i]; i++)
            ++sum;
        nuClicks = t.elapsed();
        
        //Display results
        cout << "I am finished, boss!\nThis section was running for "<< nuClicks << " seconds."<< endl;
        cout << "The sum is " << sum << endl<<endl;
        
        //**********************************
        cout << "CODE SNIPPET B" <<endl;
        sum = 0;
        t.reset();
        
        for(int i = 0; i < arr[i]; i++)
            for(int j = 0; j < arr[i]; ++j)
                ++sum;
        nuClicks = t.elapsed();
        
        //Display results
        cout << "I am finished, boss!\nThis section was running for "<< nuClicks << " seconds."<< endl;
        cout << "The sum is " << sum << endl<<endl;
        
        //**********************************
        cout << "CODE SNIPPET C" <<endl;
        sum = 0;
        t.reset();
        
        for(int i = 0; i < arr[i]; i++)
            for(int j = 0; j < i; ++j)
                ++sum;
        nuClicks = t.elapsed();
        
        //Display results
        cout << "I am finished, boss!\nThis section was running for "<< nuClicks << " seconds."<< endl;
        cout << "The sum is " << sum << endl<<endl;
        
        //**********************************
        cout << "CODE SNIPPET D" <<endl;
        sum = 0;
        t.reset();
        
        for(int i = 0; i < arr[i]; i++)
            for(int j = 0; j < arr[i]; ++j)
                for(int k = 0; k < arr[i]; ++k)
                    ++sum;
        nuClicks = t.elapsed();
        
        //Display results
        cout << "I am finished, boss!\nThis section was running for "<< nuClicks << " seconds."<< endl;
        cout << "The sum is " << sum << endl<<endl;
        
        cout << "*************************************************************\n\n" << endl;
        
    }
    
}

int main(){
    cout << "The CLOCKS PER SEC: " << CLOCKS_PER_SEC << endl << endl;
    partOne();
    partTwo();
}

//RESULTS!!!
/*
 
 The CLOCKS PER SEC: 1000000
 
 ********************************************PROGRAMMING PROBLEM ONE********************************************
 STARTING RUNTIME FOR, n =128
 Cubic Maximum Contiguous Seubsequence Sum Algorithm
 I am finished, boss!
 This section was running for 0.001655 seconds.
 The sum is 6516
 
 Quadratic Maximum Contiguous Subsequence Sum Algorithm
 I am finished, boss!
 This section was running for 6.2e-05 seconds.
 The sum is 6516
 
 Linear-time Maximum Contiguous Subsequence Sum Algorithm
 I am finished, boss!
 This section was running for 4e-06 seconds.
 The sum is 6516
 
 *************************************************************
 
 
 STARTING RUNTIME FOR, n =256
 Cubic Maximum Contiguous Seubsequence Sum Algorithm
 I am finished, boss!
 This section was running for 0.041517 seconds.
 The sum is 6516
 
 Quadratic Maximum Contiguous Subsequence Sum Algorithm
 I am finished, boss!
 This section was running for 0.000555 seconds.
 The sum is 6516
 
 Linear-time Maximum Contiguous Subsequence Sum Algorithm
 I am finished, boss!
 This section was running for 6e-06 seconds.
 The sum is 6516
 
 *************************************************************
 
 
 STARTING RUNTIME FOR, n =512
 Cubic Maximum Contiguous Seubsequence Sum Algorithm
 I am finished, boss!
 This section was running for 0.408077 seconds.
 The sum is 20818
 
 Quadratic Maximum Contiguous Subsequence Sum Algorithm
 I am finished, boss!
 This section was running for 0.002029 seconds.
 The sum is 20818
 
 Linear-time Maximum Contiguous Subsequence Sum Algorithm
 I am finished, boss!
 This section was running for 9e-06 seconds.
 The sum is 20818
 
 *************************************************************
 
 
 STARTING RUNTIME FOR, n =1024
 Cubic Maximum Contiguous Seubsequence Sum Algorithm
 I am finished, boss!
 This section was running for 3.665585 seconds.
 The sum is 21973
 
 Quadratic Maximum Contiguous Subsequence Sum Algorithm
 I am finished, boss!
 This section was running for 0.008621 seconds.
 The sum is 21973
 
 Linear-time Maximum Contiguous Subsequence Sum Algorithm
 I am finished, boss!
 This section was running for 1.9e-05 seconds.
 The sum is 21973
 
 *************************************************************
 
 
 STARTING RUNTIME FOR, n =2048
 Cubic Maximum Contiguous Seubsequence Sum Algorithm
 I am finished, boss!
 This section was running for 31.839602 seconds.
 The sum is 26642
 
 Quadratic Maximum Contiguous Subsequence Sum Algorithm
 I am finished, boss!
 This section was running for 0.03799 seconds.
 The sum is 26642
 
 Linear-time Maximum Contiguous Subsequence Sum Algorithm
 I am finished, boss!
 This section was running for 3.4e-05 seconds.
 The sum is 26642
 
 *************************************************************
 
 
 STARTING RUNTIME FOR, n =4096
 Cubic Maximum Contiguous Seubsequence Sum Algorithm
 I am finished, boss!
 This section was running for 265.603059 seconds.
 The sum is 33840
 
 Quadratic Maximum Contiguous Subsequence Sum Algorithm
 I am finished, boss!
 This section was running for 0.154692 seconds.
 The sum is 33840
 
 Linear-time Maximum Contiguous Subsequence Sum Algorithm
 I am finished, boss!
 This section was running for 9.000000000000001e-05 seconds.
 The sum is 33840
 
 *************************************************************
 
 
 ********************************************PROGRAMMING PROBLEM TWO********************************************
 STARTING RUNTIME FOR, n =256
 CODE SNIPPET A
 I am finished, boss!
 This section was running for 0 seconds.
 The sum is 6
 
 CODE SNIPPET B
 I am finished, boss!
 This section was running for 3.5e-05 seconds.
 The sum is 12032
 
 CODE SNIPPET C
 I am finished, boss!
 This section was running for 1e-06 seconds.
 The sum is 15
 
 CODE SNIPPET D
 I am finished, boss!
 This section was running for 0.109187 seconds.
 The sum is 39124992
 
 *************************************************************
 
 
 STARTING RUNTIME FOR, n =512
 CODE SNIPPET A
 I am finished, boss!
 This section was running for 0 seconds.
 The sum is 39124998
 
 CODE SNIPPET B
 I am finished, boss!
 This section was running for 3.4e-05 seconds.
 The sum is 12032
 
 CODE SNIPPET C
 I am finished, boss!
 This section was running for 0 seconds.
 The sum is 15
 
 CODE SNIPPET D
 I am finished, boss!
 This section was running for 0.10951 seconds.
 The sum is 39124992
 
 *************************************************************
 
 
 STARTING RUNTIME FOR, n =1024
 CODE SNIPPET A
 I am finished, boss!
 This section was running for 1e-06 seconds.
 The sum is 39124998
 
 CODE SNIPPET B
 I am finished, boss!
 This section was running for 3.6e-05 seconds.
 The sum is 12032
 
 CODE SNIPPET C
 I am finished, boss!
 This section was running for 1e-06 seconds.
 The sum is 15
 
 CODE SNIPPET D
 I am finished, boss!
 This section was running for 0.108402 seconds.
 The sum is 39124992
 
 *************************************************************
 
 
 STARTING RUNTIME FOR, n =2048
 CODE SNIPPET A
 I am finished, boss!
 This section was running for 1e-06 seconds.
 The sum is 39124998
 
 CODE SNIPPET B
 I am finished, boss!
 This section was running for 3.3e-05 seconds.
 The sum is 12032
 
 CODE SNIPPET C
 I am finished, boss!
 This section was running for 0 seconds.
 The sum is 15
 
 CODE SNIPPET D
 I am finished, boss!
 This section was running for 0.116794 seconds.
 The sum is 39124992
 
 *************************************************************
 
 
 STARTING RUNTIME FOR, n =4096
 CODE SNIPPET A
 I am finished, boss!
 This section was running for 1e-06 seconds.
 The sum is 39124998
 
 CODE SNIPPET B
 I am finished, boss!
 This section was running for 3.3e-05 seconds.
 The sum is 12032
 
 CODE SNIPPET C
 I am finished, boss!
 This section was running for 0 seconds.
 The sum is 15
 
 CODE SNIPPET D
 I am finished, boss!
 This section was running for 0.107431 seconds.
 The sum is 39124992
 
 *************************************************************
 
 */