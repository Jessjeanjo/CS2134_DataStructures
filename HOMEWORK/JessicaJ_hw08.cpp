//
//  JessicaJ_hw08.cpp
//
//
//  Created by Jessica Joseph on 11/28/15.
//
//

#include <iostream>
#include <string>
#include <vector>

using namespace std;


//====================================== PART ONE ======================================//



//
//
////Implement a hash table where collisions are resolved by linear probing. You will implement the methods
//find, insert, and rehash for the class below.
//1 Your class will rehash when the load factor is greater
//or equal to 0.5.
//The hash function, hf, will be created when you instantiate a member of this class. But hf will return a
//positive integer whose range is much larger than the table size. The book says, “you will need to perform
//a final mod operation internally after the” hash function.
template< class HashedObj >
class HashTable {
public:
    
    explicit HashTable( int size = 101 ):currentSize(0){ array.resize(size); }
    
    std::hash<HashedObj> hf; // create a hash function object
    
    bool find( const HashedObj & x ) const;
    void makeEmpty( );
    bool insert( const HashedObj & x );
    bool remove( const HashedObj & x);
    enum EntryType { ACTIVE, EMPTY, DELETED };
private:
    struct HashEntry
    {
        HashedObj element;
        EntryType info;
        HashEntry( const HashedObj & e = HashedObj(), EntryType i = EMPTY )
        : element( e), info(i) {}
    };
    
    vector<HashEntry> array;
    int currentSize;
    
    void rehash( );
};

template< class HashedObj >
bool HashTable<HashedObj>::find( const HashedObj & x ) const{
    
    
    
}

template< class HashedObj >
bool HashTable<HashedObj>::insert( const HashedObj & x ){
    
}

template< class HashedObj >
void HashTable<HashedObj>::rehash( ){
    
}



//====================================== PART ONE ======================================//

//2. Create an adjacency list for the graph of the NYC subway station. You will consider a subway station sa
//adjacent to subway station sb if there is a subway train that goes from sa directly to sb without going
//through any other stops, or you can transfer from sa to sb. You find what subway stations are adjacent
//to other subway stations by using the information in the files transfers.txt and MTA train stop data.
//The file transfers.txt is a bit confusing! Here are some of the entries:
//from_stop_id,to_stop_id,transfer_type,min_transfer_time
//101,101,2,180
//103,103,2,180
//104,104,2,180
//106,106,2,180
//107,107,2,180
//108,108,2,180
//109,109,2,180
//110,110,2,180
//111,111,2,180
//112,112,2,180
//112,A09,2,180
//113,113,2,180
//...
//In the file transfers.txt, observe you can transfer2
//from subway station 112 to subway station A09.
//Thus A09 is adjacent to (a neighbor of ) 112.
//You will create an adjacency list as an unordered map<string, list<string>> instead of a vector<list<string>
