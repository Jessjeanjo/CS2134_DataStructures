//
//  JessicaJ_hw04.cpp
//  
//
//  Created by Jessica Joseph on 10/5/15.
//
//

#include <iostream>
#include <cmath>
#include <vector>

using namespace std;


//********************************** PART TWO **********************************//
//a recursive function that returns the sum of the digits of a positive integer
//Optimized for negative number


//You may extend your algorithm to allow for the input to be a negative integer.
//If x = −4 then the function returns 4.
//If x = −123 then the function returns 6 = 1 + 2 + 3.

int sumDigits(int x){

        if ((x%10) == 0)
            return x%10;
        
        return sumDigits(x/10) + abs((x%10)); // abs((x%10) allows the input of negative number
    
}


//********************************** PART THREE **********************************//
//a program that sums the items in a vector using  a divide and conquer algorithm.


//Create a driver function to call the recursive function; the driver function returns the value returned
//by the recursive function.

template <class Object>
Object sumThis(const vector<Object>& a, typename vector<Object>::const_iterator begin, typename vector<Object>::const_iterator end) {
    
    if (a.size() == 1)
        return a[0];
    if (a.size() == 0)
        return 0;
    
    typename vector<Object>::const_iterator mid = begin + (end - begin)/2;
    Object leftSum = sumThis(a, begin , mid);
    Object rightSum = sumThis(a, mid, end);
    
    return leftSum + rightSum;
    
}

// Driver Function
template <class Object>
Object sumDriver(const vector<Object>& a) {
    //Base Case
    
    if (a.size() == 0)
        return 0;
    if (a.size() == 1)
        return a[0];
    
    return testTemp(a, a.begin(), a.end());
    
}


//********************************** PART FOUR **********************************//
//a program that sums the items in a vector using  a divide and conquer algorithm.


//Create a driver function to call the recursive function; the driver function returns the value returned
//by the recursive function.
//4. (Extra Credit Problem) Problem 8.25 from Weiss’ book “Data Structures and Problem Solving Using C++ (Second Edition)”:“Write the routine with the declaration: void permute( const string & str ); that prints all the permutations of the characters in the str. If str is "abc". then the strings output are abc, acb, bac, bca, cab, and cba. Use recursion.”

void permute (const string & str){
    
    for(int i = 0; i < str.length(); i++){
        
    }
}


int main(){
    int inputVal;
    
    cout << "//********************************** PART TWO **********************************//\n\nGive me any number, and I will find it's sum (even negative numbers):)" << endl;
    cin >> inputVal;
    cout << "The value of the number you input, " << inputVal << ", is " << sumDigits(inputVal) <<endl;

    cout << "\n\n//********************************* PART THREE *********************************//\n\n" << endl;

    vector<int> vecTesty {1,2,3};
    sumVector_2(vecTesty);
    
}


























