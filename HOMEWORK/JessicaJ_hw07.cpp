//
//  JessicaJ_hw07.cpp
//
//
//  Created by Jessica Joseph on 11/12/15.
//
//

#include <iostream>
#include <fstream>
#include <vector>
#include <map>

using namespace std;



//====================================== PART ONE ======================================//
//In this part, you will store a list of correctly spelled words and a point value associated with each word
//into a variable of type map<string, int>. You will perform the following steps:
//• Enter the point value from a file called Letter_point_value.txt for each letter into a variable
//of type vector<int>. The point value associated with ’A’ goes into position 0, and ’B’ goes into
//position 1, etc.
//• The point value of a word is determined by the point value of each letter. To compute the point
//value of a word, add up the point values of each letter in the word. For example, the point value of
//”cat” is 4 + 1 + 1 = 6, since ’C’ has a point value of 4, ’A’ has a point value of 1, and ’T’ has a
//point value of 1. Upper and lower case letters have the same point value. (e.g. So ”CAT” also has a
//                                                                           point value of 6.)
//Create a function to compute the point value of a word.
//• The words you will store are in a file called ENABLE.txt. You will read in each word and store it and
//its associated point value in a variable of type map<string,int>.

ifstream getValidFileStream(){
    string fileName = "Letter_point_value.txt";
    
    ifstream readingFile(fileName);
    
    while (!readingFile){
        
        cout << fileName << fileName << " was NOT found, Please make sure it is in your folder! Type in your filename!" << endl;
        getline(cin, fileName);
        readingFile.clear();
        
        ifstream readingFile(fileName);
        
    }
    
    return readingFile;
}


template <class Object>
void fillVecFromFile(vector<Object> & fillVector){
    
    string dataLine, letter;
    int point;
    
    ifstream readingFile(getValidFileStream());
    
    map<char, int> myMap;
    
    
    
    //Add some code to determine what gets filled into
    //Look at slides online to use the strinf funtion that takes up to a position...
    
    
    while ( readingFile >> point){
        
        readingFile >> letter;
        myMap.insert(std::make_pair( letter, point ));
        
        
        }
    

}





//On your own.1 You can write a program to help you play Words With Friends.
//Remember the recursive function from the extra credit problem in a previous assignment, where the user
//enters a string and you find all the combinations of the string.
//For each string created from the recursive function, you can test to see if it is in the ENABLE word list.
//If so, you print out the word and the points associated with the word. Use the map<string,int> you
//created in programming part 1.




//====================================== PART TWO ======================================//
// Add the following methods to the BinarySearchTree class.
    // Implement the find method recursively
    // Implement a method that takes two keys, low and high, and prints all the objects X that are in the range specified by low and high
    // Implement a method called NegateTree that changes every value in the tree to its negated value

//======= BINARY SEARCH TREE CLASS =======\\

#include <iostream>
#include <exception>

using namespace std;

template< class Comparable>
class BinarySearchTree;

template <class Comparable>
class BinaryNode
{
    Comparable  element;
    BinaryNode *left;
    BinaryNode *right;
    int size;
    
    BinaryNode( const Comparable & theElement, BinaryNode *lt,
               BinaryNode *rt, int sz = 1 )
    : element( theElement ), left( lt ), right( rt ), size( sz ) { }
    BinaryNode( Comparable && theElement, BinaryNode *lt,
               BinaryNode *rt, int sz = 1 )
    : element( std::move(theElement) ), left( lt ), right( rt ), size( sz ) { }
    
    friend class BinarySearchTree<Comparable>;
};

template <class Comparable>
class BinarySearchTree
{
public:
    typedef BinaryNode<Comparable> Node;
    
    BinarySearchTree( ) : root( NULL ) { } // Construct the tree.
    ~BinarySearchTree( ){ makeEmpty( ); } // Destructor for the tree.
    
    
    // add function declarations
    
    
    bool isEmpty( ) const { return root == NULL;}
    void makeEmpty( ) { makeEmpty( root ); }
    void insert( const Comparable & x ) { insert( x, root ); }
    
private:
    Node * root;
    
    // add function declarations
    
    int treeSize( Node *t ) const { return t == NULL ? 0 : t->size; }
    Comparable const * elementAt( Node *t ) const;
    void insert( const Comparable & x, Node * & t );
    void makeEmpty( Node * & t ) const;
    
};


template <class Comparable>
Comparable const * BinarySearchTree<Comparable>::elementAt( Node *t ) const
{
    if( t == NULL )
        return NULL;
    else
        return &(t->element);
}


template <class Comparable>
void BinarySearchTree<Comparable>::insert( const Comparable & x, Node * & t )
{
    if( t == NULL )
        t = new Node( x, NULL, NULL, 0 );
    else if( x < t->element )
        insert( x, t->left );
    else if( t->element < x )
        insert( x, t->right );
    else
        throw exception( );
    
    t->size++;
}


template <class Comparable>
void BinarySearchTree<Comparable>::makeEmpty( Node * & t ) const
{
    if( t != NULL )
    {
        makeEmpty( t->left );
        makeEmpty( t->right );
        delete t;
        t = NULL;
    }
}



//======= PART A =======\\
// Implement the method find recursively

template <class Object>
BinaryNode * find(BinaryNode *root, Object data){
    
    if (!root) return NULL;
    
    if (data < root->data)
        find(root->left, data);
    else if (data > root->data)
        find(root->right, data);
    
    if (data == root->data)
        return root;
}


//======= PART B =======\\
// Implement a method that takes two keys, low and high, and prints all the objects X that are in the range specified by low and high

//(b) Implement a method that takes two keys, low and high, and prints all the objects X that are in the
//range specified by low and high. Your program must run in O(k + h) time, where k is the number
//of keys printed and h is the height of the tree. Thus if k is small, you should be examining only a
//small part of the tree. Use a recursive method. Bound the running time of your algorithm using
//Big-Oh notation.
//
//

//======= PART C =======\\
// Implement a method called NegateTree that changes every value in the tree to its negated value



// (c) Create a method called negateTree that changes every value in the tree to its negated value. (The
//                                                                                                  left and right child should be swapped, thus maintaining the binary search tree)
//a
/// \
//b c
//becomes
//-a
/// \
//-c -b


//======= PART D =======\\
//(d) Create a method called average_node_depth that computes the average depth of a node in the tree.
//e.g.
//6
/// \
//3
/// \
//2 4
//\
//5
//Then the average depth of a node is (0 + 1 + 2 + 2 + 3 + 1)/6 = 9/6 since there is one node at depth
//1, two nodes at depth 2, one node at depth 3.2


//====================================== PART THREE ======================================//
//3. (Extra Credit) Run empirical studies to estimate the average height of a node in a binary search tree
//by running 100 trial of inserting n random keys into an initially empty tree. For n = 210
//, 2
//11
//, 2
//12. For
//each n print the min, the max, and the average.

//
//
//int main ( ){
//    
//    cout << "\n\n//********************************* PART ONE *********************************//\n\n" << endl;
//    cout << "\n\n//********************************* PART TWO *********************************//\n\n" << endl;
//    
//    return 0;
//}