//
//  HW05_WQ_scrap.cpp
//  
//
//  Created by Jessica Joseph on 10/18/15.
//
//

#include <iostream>
#include <vector>

using namespace std;

template <class Comparable>
void printVec(vector<Comparable> &  vecYo){
    for (int i = 0; i < 7; i++){
        cout << vecYo[i] << endl;
    }
}


template <class Comparable>
void mergeSort( vector<Comparable> & a, vector<Comparable> & tmpArray, int left, int right )
{
    if( left < right )
    {
        int center = ( left + right ) / 2;
        mergeSort( a, tmpArray, left, center );
        mergeSort( a, tmpArray, center + 1, right );
        merge( a, tmpArray, left, center + 1, right );
        printVec(a); // prints the contents of the vector in order
    }
}

int main () {
    vector<int> thisVec{9, 8, 11, 2, 0, 3}, tmpArr{};
    
    mergeSort(thisVec, tmpArr, 0, 6);
}