//
//  minion.cpp
//  MAIN_2134
//
//  Created by Jessica Joseph on 11/24/15.
//  Copyright (c) 2015 Jessica Joseph. All rights reserved.
//

#include "minion.h"
#include <iostream>
#include <string>

using namespace std;


class Minion{

    int feet;
    string eye;
    string skin;
    string clothes;
    
public:
    
    
    Minion(){
        
        feet = 4;
        eye =  "brown";
        skin = "yellow";
        clothes = "overalls";
        
    }
    
    
    
    void walk(){cout << "Hey, I am walking" << endl;}
    void talk() {cout << "HEy I'm talking" << endl;}
    
    void changeSkin(string color){skin = color;}
    string getSkin() {return skin;}
    
};



int main(){
    
    
    Minion Jessica;
    Minion Gabby;
    
    cout << Jessica.getSkin() << endl;
    cout << Gabby.getSkin() << endl;

    Jessica.walk();
    Jessica.talk();
    
    
    Jessica.changeSkin("blue");
    
    cout << Jessica.getSkin() << endl;
    cout << Gabby.getSkin() << endl;

    
    return 0;
}

//