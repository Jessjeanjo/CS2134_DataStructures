//
//  JessicaJ_hw02.cpp
//
//
//  Created by Jessica Joseph on 9/17/15.
//
//

#include "JessicaJ_hw02.h"

ifstream getValidFileStream(){
    string fileName = "MTA_train_stop_data.txt";
    
    ifstream readingFile(fileName);
    
    while (!readingFile){
        
        cout << fileName << " NOT found, Please make sure it is in your folder! Type in your filename!" << endl;
        getline(cin, fileName);
        readingFile.clear();
        
        ifstream readingFile(fileName);
        
    }
    
    return readingFile;
}


template <class Object>
void fillVecFromFile(vector <Object> & fillVector){
    
    int newElement;
    ifstream readingFile(getValidFileStream());
    
    string stopID, stopName, headerLine, dataLine;
    double stopLat, stopLong;
    
    //Add some code to determine what gets filled into
    //Look at slides online to use the strinf funtion that takes up to a position...
    
    getline(readingFile, headerLine);
    vector<string> fields(10);
    string ObjectArr[5];
    
    while (getline(readingFile, dataLine)){
        string s = "101,,Van Cortlandt Park - 242 St,,40.889248,-73.898583,,,1,";
        
        //Could use recursion
        int pos1 = dataLine.find_first_of(',', 0);
        fields[0] = dataLine.substr(0, pos1);
        
        int pos2 = dataLine.find_first_of(',', pos1+1);
        fields[1]= dataLine.substr(pos1+1, pos2-pos1-1);
        
        int pos3 = dataLine.find_first_of(',', pos2+1);
        fields[2]= dataLine.substr(pos2+1, pos3-pos2-pos1-1);
        
        int pos4 = dataLine.find_first_of(',', pos3+1);
        fields[3]= dataLine.substr(pos2+1, pos4-pos3-pos2-pos1-1);
        
        int pos5 = dataLine.find_first_of(',', pos2+1);
        fields[4]= dataLine.substr(pos2+1, pos5-pos4-pos3-pos2-pos1-1);
        
        int pos6 = dataLine.find_first_of(',', pos2+1);
        fields[5]= dataLine.substr(pos2+1, pos6-pos5-pos4-pos3-pos2-pos1-1);
        
        int pos7 = dataLine.find_first_of(',', pos2+1);
        fields[6]= dataLine.substr(pos2+1, pos7-pos6-pos5-pos4-pos3-pos2-pos1-1);
        
        int pos8 = s.find_first_of(',', pos2+1);
        fields[7]= s.substr(pos2+1, pos8-pos7-pos6-pos5-pos4-pos3-pos2-pos1-1);
        
        int pos9 = dataLine.find_first_of(',', pos2+1);
        fields[8]= dataLine.substr(pos2+1, pos9-pos8-pos7-pos6-pos5-pos4-pos3-pos2-pos1-1);
        
        int counter = 0;
        for (int i = 0; i <9; i++){
            if (fields[i] != ""){
                ObjectArr[counter] = fields[i];
                counter++;
            }
        }
        
        fillVector.push_back(TrainStopData(ObjectArr[0], ObjectArr[1], ObjectArr[2], ObjectArr[3]));
        
    }
}


int main(){
    
    vector<TrainStopData()> trainStopVec;
    fillVecFromFile(trainStopVec);
    
}