//
//  JessicaJ_hw05.cpp
//
//
//  Created by Jessica Joseph on 10/25/15.
//
//

#include <iostream>
#include <vector>
#include <list>

using namespace std;

//=========================================== LIST CLASS ===========================================\\

template <typename Object>
class List
{
private:
    
    struct Node
    {
        Object  data;
        Node   *next;
        
        Node( const Object & d = Object{ },  Node * n = nullptr )
        : data{ d },  next{ n } { }
        
        Node( Object && d, Node * n = nullptr )
        : data{ std::move( d ) }, next{ n } { }
    };
    
public:
    class iterator
    {
    public:
        
        iterator( ): current( nullptr )
        { }
        
        Object & operator* ( )
        { return current->data; }
        
        const Object & operator* ( ) const
        { return  current->data; }
        
        iterator & operator++ ( )
        {
            this->current = this->current->next;
            return *this;
        }
        
        iterator operator++ ( int )
        {
            iterator old = *this;
            ++( *this );
            return old;
        }
        
        bool operator== ( const iterator & rhs ) const
        { return current == rhs.current; }
        
        bool operator!= ( const iterator & rhs ) const
        { return !( *this == rhs ); }
        
    private:
        Node * current;
        
        iterator( Node *p ) : current{ p }
        { }
        
        friend class List<Object>;
    };
    
public:
    List( )
    { header = new Node; }
    
    ~List( )
    {
        clear( );
        delete header;
    }
    
    List( const List & rhs );
    
    List & operator= ( const List & rhs )
    {
        List copy = rhs;
        swap( *this, copy );
        return *this;
    }
    
    List( List && rhs ):header{ new Node }
    {
        header->next = rhs.header->next;
        rhs.header->next = nullptr;
    }
    
    List & operator= ( List && rhs )
    {
        swap( header, rhs.header );
        return *this;
    }
    
    iterator begin( ) const
    { return iterator( header->next ); }
    
    iterator end( ) const
    { return iterator( nullptr ); }
    
    iterator before_begin( ) const
    { return iterator( header ); }
    
    bool empty( ) const
    { return header->next == nullptr; }
    
    void clear( )
    {
        while( !empty( ) )
            pop_front( );
    }
    
    void pop_front( )
    { erase_after( before_begin( ) ); }
    
    iterator insert_after( iterator itr, const Object & x )
    {
        Node *p = itr.current;
        p->next = new Node{ x, p->next };
        
        return iterator(p->next);
    }
    
    void remove( const Object & x )
    {
        Node * prev = header;
        
        while ( prev->next != nullptr )
        {
            if ( prev->next->data == x )
                erase_after( iterator(prev) );
            else
                prev = prev->next;
        }
    }
    
    iterator erase_after( iterator itr )
    {
        Node *p = itr.current;
        Node *oldNode = p->next;
        
        p->next = oldNode->next;
        delete oldNode;
        
        return iterator( p->next );
    }
    
    Object & front( );
    
    const Object & front( ) const;
    
    void merge( List & alist );
    
    void reverse( );
    
    template<class Predicate>
    void remove_if( Predicate pred );
    
    iterator insert_after( iterator itr, Object && x );
    
private:
    Node *header;
    
};


//********************************** PART ONE **********************************//

//PART A
template <typename Object>
List<Object>::List( const List<Object> & rhs ){
    header = new Node(rhs, rhs->next);
    Node * tracker = header;
    
    while ( tracker != NULL){
        tracker->next;
    }
}

//PART B
template <typename Object>
Object & List<Object>::front(){
    return header->data;
}

//template <typename Object>
//const Object & List<Object>::front() const{
//    return header;
//}


//PART C
template <typename Object>
void List<Object>::merge( List<Object> & alist ){
    insert_after(end(), alist.before_begin());
}

//PART D
template <typename Object>
void List<Object>::reverse( ){

    if (empty())
        return;
    
    Node* first;
    Node* rest;
    
    while (rest->next != NULL){
        
        
    }

}

//PART E
template <typename Object>
typename List<Object>::iterator List<Object>::insert_after( iterator itr, Object && x ){
    return iterator(x->next->next);
}

//PART F
template <typename Object>
template <class Predicate>
void List<Object>::remove_if( Predicate pred ){

}
