////
//////
//////  scrap.cpp
//////
//////
//////  Created by Jessica Joseph on 9/28/15.
//////
//////
////
////#include <iostream>
////#include <vector>
////
////using namespace std;
//////
//////Given an unsorted array. Sort the array without comparing. It should be done in O(n) time
//////There only exists 5 #’s in the entire
////
////
//////int main(){
//////
//////    vector<int> c {1,2,3,4,5,6,7,8,9,10};
//////    vector<int>::iterator first = c.begin()+2;
//////    vector<int>::iterator last = c.end();
//////        cout << *(first + (last  - first )/2 ) << endl;
//////}
////
//////
////void rec(int & x){
////
////    if (x != 0){
////        return ;}
////
////    //cout << x;
////    x--;
////
////    rec(x);
////    //cout << x;
////
////    x--;
////
////    rec(x);
////    cout << x;
////
////}
////
////int main(){
////    int x = 5;
////    rec(x);
////
////}
//////
//////void recursiveReverse(struct node** head_ref)
//////{
//////    struct node* first;
//////    struct node* rest;
//////    if (*head_ref == NULL)
//////    return;
//////
//////    first = *head_ref;
//////    rest  = first->next;
//////
//////    if (rest == NULL)
//////    return;
//////
//////    recursiveReverse(&rest);
//////
//////    first->next->next  = first;
//////
//////    first->next  = NULL;
//////
//////    *head_ref = rest;
//////}
////
////
////
/////=========================================== LINKED LIST CLASS ===========================================\\
////// USING LINKED LIST WHICH I CREATED IN CS1124 \\
////
////template <class Object>
////
////class LList{
////    template <class T>
////    class LListNode{
////        LListNode<T>* next;
////    public:
////        T data;
////        LListNode(T newdata = T(), LListNode<T>* newNext = NULL) :data(newdata), next(newNext){}
////        LListNode( T && d, LListNode * n = nullptr ): data( move( d ) ), next( n ) { }
////        friend class LList < T > ;
////        void listDisplay() const
////        {
////            cout << data << ' ';
////            LListNode<T>* p = next;
////            while (p != nullptr) {
////                cout << p->data << ' ';
////                p = p->next;
////            }
////            cout << endl;
////        }
////
////    };
////
////    LListNode<Object>* head;
////    LListNode<Object>* recursiveCopy(LListNode<Object>* rhs){
////        if (rhs == NULL)
////            return NULL;
////        return new LListNode<Object>(rhs->data, recursiveCopy(rhs->next));
////    }
////
////
////public:
////
////    class iterator
////    {
////    public:
////
////        iterator( ): current( nullptr )
////        { }
////
////        Object & operator* ( )
////        { return current->data; }
////
////        const Object & operator* ( ) const
////        { return  current->data; }
////
////        iterator & operator++ ( )
////        {
////            this->current = this->current->next;
////            return *this;
////        }
////
////        iterator operator++ ( int )
////        {
////            iterator old = *this;
////            ++( *this );
////            return old;
////        }
////
////        bool operator== ( const iterator & rhs ) const
////        { return current == rhs.current; }
////
////        bool operator!= ( const iterator & rhs ) const
////        { return !( *this == rhs ); }
////
////    private:
////        LListNode<Object> * current;
////
////        iterator( LListNode<Object> *p ) : current{ p }
////        { }
////
////        friend class LList<Object>;
////    };
////
////
////public:
////    LList() :head(NULL){}
////    ~LList(){ clear(); }
////    LList(const LList<Object>& rhs) :head(NULL){ *this = rhs; }
////    bool isEmpty(){ return head == NULL; }
////
////
////    void push_front(const Object& newdata){
////        head = new LListNode<Object>(newdata, head);
////    }
////
////
////
////    Object pop_front(){
////        if (isEmpty()) // pop on an empty list????
////            return Object();
////        Object retval = head->data;
////        LListNode<Object>* temp = head;
////        head = head->next;
////        delete temp;
////        return retval;
////    }
////
////
////    void clear(){ while (!isEmpty()) pop_front(); }
////    int size()const{
////        int count = 0;
////        for (LListNode<Object>* temp = head; temp != NULL; temp = temp->next)
////            count++;
////        return count;
////
////    }
////
////
////    LListNode<Object>* find(Object toFind){
////        LListNode<Object>* temp = head;
////        for (; temp != NULL&& temp->data != toFind; temp = temp->next);
////        return temp;
////    }
////
////
////    void push_back(const Object& newdata){
////        if (isEmpty())
////            push_front(newdata);
////        else{
////            LListNode<Object>* temp=head;
////            while (temp->next != NULL)
////                temp = temp->next;
////            push_after(newdata, temp);
////        }
////    }
////
////
////
////    Object pop_back(){
////        if (isEmpty())
////            return Object();
////        else if (head->next == NULL)
////            return pop_front();
////        LListNode<Object>* temp = head;
////        while (temp->next->next != NULL) //stops on second to last node
////            temp = temp->next;
////        Object retval = temp->next->data;
////        LListNode<Object>* toDelete = temp->next;
////        temp->next = NULL;
////        delete toDelete;
////        return retval;
////    }
////
////
////    void push_after(Object newElement, LListNode<Object>* ptr){
////        ptr->next = new LListNode<Object>(newElement, ptr->next);}
////
////    void splice(LListNode<Object> * insertAf, LList<Object>* newList){
////
////        LListNode<Object> *currList = head, *temp = find(insertAf->data);
////
////        temp->next = newList->head;
////
////        while (currList->next != NULL)
////            currList = currList->next;
////
////        find(currList->data)->next = temp;
////
////    }
////
////
////    LListNode<Object>* subList(LList<Object>& bigList, LList<Object>& checkList);
////    LListNode<Object>* getHead() const { return head; }
////
////
////    LList<Object>& operator=(const LList<Object>& rhs){
////        if (this == &rhs)
////            return *this;
////        clear();
////
////        head = recursiveCopy(rhs.head);
////        return *this;
////    }
////
////    LListNode<Object>* listSearch(LList<Object>* sub);
////
////};
////
////
////
//
//
//
//
//
//
//
//
//
//// CS2134 Fall 2015
//// Homework 5 Programming Solution
//
//#include <iostream>
//#include <algorithm>
//#include <stack>
//#include <string>
//using namespace std;
//
//// This is a dummy functor I wrote to test remove_if
//class HowOdd {
//public:
//    bool operator() (const int& val) { return val % 2; }
//};
//
///////////////////////////////////////////////////////////////////
//// PROVIDED LIST CODE
///////////////////////////////////////////////////////////////////
//
//template <class Object>
//class List {
//private:
//    
//    struct Node {
//        Object  data;
//        Node    *next;
//        
//        Node (const Object& d = Object (),  Node* n = nullptr)
//        : data{ d },  next{ n } { }
//        
//        Node (Object&& d, Node* n = nullptr)
//        : data{ std::move( d ) }, next{ n } { }
//    };
//    
//    Node *header;
//    
//public:
//    class iterator {
//    public:
//        
//        iterator(): current( nullptr ) {}
//        
//        Object & operator* ( ) { return current->data; }
//        
//        const Object & operator* ( ) const { return  current->data; }
//        
//        iterator & operator++ () {
//            this->current = this->current->next;
//            return *this;
//        }
//        
//        iterator operator++ (int) {
//            iterator old = *this;
//            ++(*this);
//            return old;
//        }
//        
//        bool operator== (const iterator & rhs) const { return current == rhs.current; }
//        
//        bool operator!= (const iterator & rhs) const { return !( *this == rhs ); }
//        
//    private:
//        Node * current;
//        iterator( Node *p ) : current{ p } {}
//        friend class List<Object>;
//    };
//    
//    List() { header = new Node; }
//    
//    ~List() {
//        clear();
//        delete header;
//    }
//    
//    List & operator= ( const List & rhs ) {
//        List copy = rhs;
//        std::swap( *this, copy );
//        return *this;
//    }
//    
//    List( List && rhs ): header{ new Node } {
//        header->next = rhs.header->next;
//        rhs.header->next = nullptr;
//    }
//    
//    List & operator= ( List && rhs ) {
//        std::swap( header, rhs.header );
//        return *this;
//    }
//    
//    iterator begin () const { return iterator( header->next ); }
//    
//    iterator end () const { return iterator( nullptr ); }
//    
//    iterator before_begin () const { return iterator( header ); }
//    
//    bool empty () const { return header->next == nullptr; }
//    
//    void clear () {
//        while(!empty())
//            pop_front();
//    }
//    
//    void pop_front () { erase_after( before_begin( ) ); }
//    
//    iterator insert_after( iterator itr, const Object & x ) {
//        Node *p = itr.current;
//        p->next = new Node{ x, p->next };
//        return iterator(p->next);
//    }
//    
//    void remove( const Object & x ) {
//        Node * prev = header;
//        while ( prev->next != nullptr ) {
//            if ( prev->next->data == x )
//                erase_after( iterator(prev) );
//            else
//                prev = prev->next;
//        }
//    }
//    
//    iterator erase_after( iterator itr ) {
//        Node *p = itr.current;
//        Node *oldNode = p->next;
//        
//        p->next = oldNode->next;
//        delete oldNode;
//        
//        return iterator( p->next );
//    }
//    
//    // Wrote for myself for debugging
//    void printInOrder () const {
//        cout << "The current list: ";
//        Node* temp = header->next;
//        while (temp != nullptr) {
//            cout << temp->data << " ";
//            temp = temp->next;
//        }
//        cout << endl;
//    }
//    
//    /////////////////////////////////////////////////////////////////
//    /////////////////////////////////////////////////////////////////
//    // THINGS TO IMPLEMENT IN THE HOMEWORK
//    /////////////////////////////////////////////////////////////////
//    /////////////////////////////////////////////////////////////////
//    
//    // PART A: COPY CONSTRUCTOR
//    // Note that this is a deep copy and shouldn't affect the passed in list
//    
//    List (const List& rhs) : header(new Node) {
//        Node* curr = header;
//        for (iterator temp = rhs.begin(); temp != rhs.end(); temp++) {
//            curr->next = new Node(*temp, nullptr);
//            curr = curr->next;
//        }
//    }
//    
//    // PART B: FRONT()
//    // Returns a direct reference to the element in the first node
//    // The STL specifies undefined behavior if the list is empty
//    
//    Object& front () {
//        return *begin();
//    }
//    
//    const Object& front() const {
//        return *begin();
//    }
//    
//    // PART C: MERGE()
//    // You're allowed to assume that both lists are sorted already
//    
//    void merge(List & alist) {
//        Node* lhsprev = header;
//        Node* lhscurr = header->next;
//        Node* rhscurr = alist.header->next;
//        while (lhscurr != nullptr && rhscurr != nullptr) {
//            if (lhscurr->data < rhscurr->data) {
//                lhscurr = lhscurr->next;
//                lhsprev = lhsprev->next;
//            } else {
//                Node* temp = rhscurr->next;
//                rhscurr->next = lhscurr;
//                lhsprev->next = rhscurr;
//                rhscurr = temp;
//                lhsprev = lhsprev->next;
//            }
//        }
//        while (rhscurr != nullptr) {
//            Node* temp = rhscurr->next;
//            rhscurr->next = nullptr;
//            lhsprev->next = rhscurr;
//            rhscurr = temp;
//        }
//        alist.header->next = nullptr;
//    }
//    
//    // PART D: REVERSE()
//    
//    // This will reverse the order of the elements in the list
//    // This should NOT invalidate iterators
//    
//    // There are *many* ways to implement this, whether you
//    // use a stack, do it recursively, or just keep track of
//    // multiple pointers. Below is just one approach, which
//    // makes use of stacks. (This may not be the *most* efficient
//    // approach, but is highly recommended by Professor Sellie,
//    // since it very nicely simplifies the problem and makes use of
//    // other data structures!)
//    
//    void reverse() {
//        Node* p = header->next;
//        stack<Node*> s;
//        while(p->next != nullptr){
//            s.push(p);
//            p = p->next;
//        }
//        header->next = p;
//        while(!s.empty()){
//            p->next = s.top();
//            p = p->next;
//            s.pop();
//        }
//        p->next = nullptr;
//    }
//    
//    // PART E: INSERT_AFTER()
//    // Note that this works on rvals (but the core code is the same as
//    // the provided insert_after())
//    
//    iterator insert_after (iterator itr, Object&& x) {
//        Node *p = itr.current;
//        p->next = new Node{ x, p->next };
//        return iterator(p->next);
//    }
//    
//    // PART F: REMOVE_IF()
//    // Uses erase_after() to remove *ALL* elements that match the functor pred
//    
//    template<class Predicate>
//    void remove_if (Predicate pred) {
//        iterator prev = before_begin();
//        iterator curr = begin();
//        while (curr != end()) {
//            if (pred(*curr)) {
//                curr = erase_after(prev);
//            } else {
//                prev++;
//                curr++;
//            }
//        }
//    }
//};
//
///////////////////////////////////////////////////////////////////
//// Test code for the implemented functions
///////////////////////////////////////////////////////////////////
//
//int main() {
//    
//    string determine;
//    cout << "sdfs" <<endl;
//    
//    if (strncmp((cin >> determine), "encrypt"))
//        
//        //    // Create the list
//        //    List<int> a;
//        //
//        //    // Note that I created a printInOrder() function for debugging
//        //    cout << "\nInitialized empty list:\n(A) ";
//        //    a.printInOrder();
//        //
//        //    // Test out the provided insert_after()
//        //    int thing = 1;
//        //    cout << "\nUsing the provided insert_after():" << endl;
//        //    cout << "Insert " << thing << endl;
//        //    a.insert_after(a.before_begin(), thing);
//        //    cout << "(A) ";
//        //    a.printInOrder();
//        //
//        //    // Part B: Test the functionality of front()
//        //    cout << "\nUsing front():" << endl;
//        //    cout << "Set front to 5" << endl;
//        //    a.front() = 5;
//        //    cout << "Front element is: " << a.front() << endl;
//        //    cout << "(A) ";
//        //    a.printInOrder();
//        //
//        //    // Part E: Test the functionality of insert_after() with rval ref
//        //    cout << "\nUsing insert_after() that we've implemented:" << endl;
//        //    cout << "Insert 3 after 5" << endl;
//        //    a.insert_after(a.begin(), 3);
//        //    cout << "(A) ";
//        //    a.printInOrder();
//        //    cout << "Insert 7 at the front" << endl;
//        //    a.insert_after(a.before_begin(), 7);
//        //    cout << "(A) ";
//        //    a.printInOrder();
//        //
//        //    // Part D: Test the functionality of reverse()
//        //    cout << "\nUsing reverse():" << endl;
//        //    cout << "(A) ";
//        //    a.printInOrder();
//        //    List<int>::iterator check = a.begin();
//        //    cout << "Iterator 'check' originally points to: " << *check << endl;
//        //    cout << "Now, reverse the list:" << endl;
//        //    a.reverse();
//        //    cout << "(A) ";
//        //    a.printInOrder();
//        //    cout << "Iterator 'check' now points to: " << *check << endl;
//        //
//        //    // Part A: Copy Constructor
//        //    cout << "\nCopy constructor:" << endl;
//        //    List<int> b(a);
//        //    cout << "(A) ";
//        //    a.printInOrder();
//        //    cout << "(B) ";
//        //    b.printInOrder();
//        //
//        //    // Add some stuff to B
//        //    cout << "\nInsert more things into B:" << endl;
//        //    // b.insert_after(b.before_begin(), 14);
//        //    b.insert_after(b.before_begin(), 11);
//        //    b.insert_after(b.before_begin(), 8);
//        //    b.insert_after(b.before_begin(), 9);
//        //    b.insert_after(b.before_begin(), 4);
//        //    // b.insert_after(b.before_begin(), 2);
//        //    cout << "(A) ";
//        //    a.printInOrder();
//        //    cout << "(B) ";
//        //    b.printInOrder();
//        //
//        //    // Part F: remove_if()
//        //    cout << "\nPerform remove_if on B:" << endl;
//        //    b.remove_if(HowOdd());
//        //    cout << "(A) ";
//        //    a.printInOrder();
//        //    cout << "(B) ";
//        //    b.printInOrder();
//        //    
//        //    // Part C: merge()
//        //    // We're assuming that both lists are already sorted
//        //    // In this case, the two lists happen to be in ascending order, so this works
//        //    cout << "\nPerform merge():" << endl;
//        //    a.merge(b);
//        //    cout << "(A) ";
//        //    a.printInOrder();
//        //    cout << "(B) ";
//        //    b.printInOrder();
//        //    cout << endl;
//        }
//
//

#include <iostream>
using namespace std;

int main() {
    // your code goes here
    
    int myarr[5] = {10, 20, 30, 40, 50};
    
    int *arrItr = &myarr[0];
    
    arrItr += 2;

    cout << *arrItr++ << endl;
    cout << *++arrItr << endl;
}

